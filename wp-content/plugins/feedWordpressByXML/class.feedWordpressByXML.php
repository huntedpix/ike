<?php

/*
 * Manage admin panel & options
 */

class FeedWordpressByXML {

  public function plugin_activation() {
    // Hook cron
    wp_schedule_event( current_time( 'timestamp' ), 'hourly', 'feedWordpressByXMLCron');

    // Add option to database to know when last update was done.
    add_option('feedWordpressByXML_last_update', '', '', 'no');

    // Load translation
    load_plugin_textdomain( 'feedWordpressByXML' );
  }

  public function plugin_deactivation() {
    // Remove cron
    wp_clear_scheduled_hook('feedWordpressByXMLCron');

    // Remove option
    delete_option('feedWordpressByXML_last_update');
  }

  public static function update() {
    global $wpdb;

    // Load translation
    load_plugin_textdomain( 'feedWordpressByXML' );

    $getter = new feedWordpressByXML_Getter();
    $postHandler = new feedWordpressByXML_PostHandler();

    $content = $getter->getXMLContent();

    // Check if content was found
    if( !$content ){
      $GLOBALS['msg']->setMessage(__('Can\'t get jobs content. Please try later and if the problem persist contact your administrator.', 'feedWordpressByXML'), 'error');
      return false;
    }

    $items = feedWordpressByXML_Parser::parseXML($content);
    try{
      $wpdb->query('START TRANSACTION');

      $postHandler->init($items);
      $GLOBALS['msg']->setMessage(__('Jobs posts has been updated', 'feedWordpressByXML'));


      // SQL Transaction
      $wpdb->query('COMMIT');
    } catch(Exception $e) {
      // SQL Transaction
      $wpbd->query('ROLLBACK');

      $GLOBALS['msg']->setMessage($e->getMessage(), 'error');
    }
    // try{
    //   // SQL Transaction, 2nd arg set to use current db connection
    //   mysql_query("BEGIN", $wpdb->dbh);
    //
    //   $postHandler->init($items);
    //   $GLOBALS['msg']->setMessage(__('Jobs posts has been updated', 'feedWordpressByXML'));
    //
    //
    //   // SQL Transaction
    //   mysql_query("COMMIT", $wpdb->dbh);
    // } catch(Exception $e) {
    //   // SQL Transaction
    //   mysql_query("ROLLBACK", $wpdb->dbh);
    //
    //   $GLOBALS['msg']->setMessage($e->getMessage(), 'error');
    // }

    // Update last update date.
    update_option('feedWordpressByXML_last_update', current_time( 'timestamp' ));
  }

}
