<?php

/*
 * Parse XML content
 */

class feedWordpressByXML_Parser {

  private $defaultUserID = 2; // Elsa Reynaud
  private $users = null;
  private $categories = null;

  function __construct() {
    $this->setValuesToUsers();
    $this->setValueToCategories();
  }

  public function setValuesToUsers() {
    // Get all wp user
    $user_query = new WP_User_Query(array(
      'fields' => array( 'id', 'display_name' ),
      'orderby' => 'id'
    ));

    $this->users = $user_query->results;
  }

  public function setValueToCategories() {
    $args = array(
        'orderby'       => 'id',
        'hide_empty'    => false,
        'fields'        => 'id=>name',
    );

    $this->categories = get_terms( FEEDWORDPRESSBYXML_CT_NAME, $args );
  }

  public static function parseXML($xml) {
    $xml = new SimpleXMLElement($xml);

    return $xml;
  }

  public function getId($item) {
    return (string) $item->id;
  }

  public function getTitle($item) {
    return (string) $item->title;
  }

  public function getDescription($item) {
    return (string) $item->mission;
  }

  public function getExcerpt($item) {
    return (string) $item->teaser;
  }

  public function getKeywords($item) {
    return $item->keywords;
  }

  public function getCountry($item) {
    return $item->country;
  }

  public function getArea($item) {
    return $item->area;
  }

  public function getCity($item) {
    return $item->city;
  }

  public function getRecruiter($item) {
    return $item->recruiter;
  }

  public function getPublishedStatus($item) {
    return (int) $item->published;
  }

  public function getPublicationDate($item) {
    return $item->publication_date;
  }

  public function getExternalID($item) {
    return $item->external_id;
  }

  public function getExternalEmail($item) {
    return $item->external_email;
  }

  public function getReference($item) {
    return $item->reference;
  }

  public function getIndustry($item) {
    return $item->industry;
  }

  public function getSalaryInfos($item) {
    return $item->salary;
  }

  public function getSalaryMin($item) {
    return $this->getSalaryInfos($item)->min;
  }

  public function getSalaryMax($item) {
    return $this->getSalaryInfos($item)->max;
  }

  public function getPeriod($item) {
    return $this->getSalaryInfos($item)->period;
  }

  public function getAdvantages($item) {
    return $this->getSalaryInfos($item)->advantages;
  }

  public function getAuthorID($item) {
    $id = $this->defaultUserID;

    // Get Recruiter name from xml
    $xmlRecruiter = $this->getRecruiter($item);

    // User Loop
    if (!empty($this->users)) {
      foreach ($this->users as $user) {
        if((string) $xmlRecruiter === $user->display_name) {
          $id = $user->id;
          break;
        }
      }
    }
    return $id;
  }

  public function getCategories($item) {
    $xmlCategories = $this->getKeywords($item);

    // IF no keywords return empty array
    if((string) $xmlCategories === ''){
      return array();
    }

    // Create an array with the keywords
    $xmlCategories = explode(", ", trim((string) $xmlCategories, ', '));

    // Check if the keywords already exist if not create it
    $this->checkIfCategoriesExist($xmlCategories);

    // Now we are sure all keywords exists -> get the id and return it
    return $this->getCategoriesIdFromName($xmlCategories);
  }

  /*
   * Check if all the categories from the current keywords exist
   * If not, the add them
   * After all update private $categories
   * /!\ Case sensitive -> maybe #TODO ?
   * return void
   */
  public function checkIfCategoriesExist($xmlCategories) {
    $update = false;

    // Check if the category already exist or not
    $newCategories = array();
    for ($i = 0, $l = count($xmlCategories); $i < $l; $i++) {
      if( !in_array($xmlCategories[$i], $this->categories) ){
        $newCategories[] = $xmlCategories[$i];
      }
    }
    // Be sure we don't have duplicate entries
    $newCategories = array_unique($newCategories);

    for ($i = 0, $l = count($newCategories); $i < $l; $i++) {
      wp_insert_term($newCategories[$i], FEEDWORDPRESSBYXML_CT_NAME);
      $update = true;
    }

    // As we add some items we need to update it
    if($update){
      $this->setValueToCategories();
    }
  }

  public function getCategoriesIdFromName($categories) {
    $ids = array();
    for($i = 0; $i < count($categories); $i++){
      $term = get_term_by('name', $categories[$i], FEEDWORDPRESSBYXML_CT_NAME);
      $ids[$i] = $term->term_id;
    }
    return $ids;
  }

}
