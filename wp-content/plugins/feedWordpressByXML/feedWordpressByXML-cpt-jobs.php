<?php

/*
 * Add custom post type to wordpress.
 * Here is the jobs custom post type creation and settings
 * Jobs custom post type will be used to display jobs from braodbean feed
 */

// Register Jobs post
function register_cpt_jobs() {

  // Set labels
  $labels = array(
      'name' => _x('Jobs', 'Post Type General Name', 'feedWordpressByXML'),
      'singular_name' => _x('Job', 'Post Type Singular Name', 'feedWordpressByXML'),
      'menu_name' => __('Jobs', 'feedWordpressByXML'),
      'parent_item_colon' => __('Parent Job:', 'feedWordpressByXML'),
      'all_items' => __('All Jobs', 'feedWordpressByXML'),
      'view_item' => __('View Job', 'feedWordpressByXML'),
      'add_new_item' => __('Add New Jobs', 'feedWordpressByXML'),
      'add_new' => __('Add New Jobs', 'feedWordpressByXML'),
      'edit_item' => __('Edit Job', 'feedWordpressByXML'),
      'update_item' => __('Update Job', 'feedWordpressByXML'),
      'search_items' => __('Search Jobs', 'feedWordpressByXML'),
      'not_found' => __('Not Jobs found', 'feedWordpressByXML'),
      'not_found_in_trash' => __('Not Jobs found in Trash', 'feedWordpressByXML'),
  );

  // Set Rewrite settings
  $rewrite = array(
      'slug' => FEEDWORDPRESSBYXML_CPT_NAME,
      'with_front' => true,
      'pages' => true,
      'feeds' => true,
  );

  // Than set post_type settings add labels and rewrite settings
  $args = array(
      'label' => __('jobs', 'feedWordpressByXML'),
      'description' => __('Jobs offer', 'feedWordpressByXML'),
      'labels' => $labels,
      'supports' => array('title', 'editor', 'excerpt', 'author',),
      'taxonomies' => array(FEEDWORDPRESSBYXML_CT_NAME),
      'hierarchical' => false,
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => false,
      'show_in_admin_bar' => false,
      'menu_position' => 5,
      'can_export' => true,
      'has_archive' => true,
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'rewrite' => $rewrite,
      'capability_type' => 'post',
  );

  // Finaly register post type
  register_post_type(FEEDWORDPRESSBYXML_CPT_NAME, $args);
}

// Hook into the 'init' action
add_action('init', 'register_cpt_jobs', 0);

// Register Custom Jobs Taxonomy
function jobs_taxonomy() {

  // Set labels
  $labels = array(
      'name' => _x('Categories', 'Taxonomy General Name', 'feedWordpressByXML'),
      'singular_name' => _x('Category', 'Taxonomy Singular Name', 'feedWordpressByXML'),
      'menu_name' => __('Categories', 'feedWordpressByXML'),
      'all_items' => __('All Categories', 'feedWordpressByXML'),
      'parent_item' => __('Parent Category', 'feedWordpressByXML'),
      'parent_item_colon' => __('Parent Category:', 'feedWordpressByXML'),
      'new_item_name' => __('New Category Name', 'feedWordpressByXML'),
      'add_new_item' => __('Add New Category', 'feedWordpressByXML'),
      'edit_item' => __('Edit Category', 'feedWordpressByXML'),
      'update_item' => __('Update Category', 'feedWordpressByXML'),
      'separate_items_with_commas' => __('Separate Categories with commas', 'feedWordpressByXML'),
      'search_items' => __('Search Categories', 'feedWordpressByXML'),
      'add_or_remove_items' => __('Add or remove Categories', 'feedWordpressByXML'),
      'choose_from_most_used' => __('Choose from the most used Categories', 'feedWordpressByXML'),
      'not_found' => __('Not Found', 'feedWordpressByXML'),
  );
  
  // Set Rewrite settings
  $rewrite = array(
      'slug' => 'filter',
      'with_front' => true,
      'hierarchical' => true,
  );
  
  // Than set taxonomy settings add labels and rewrite settings
  $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'public' => true,
      'show_ui' => true,
      'show_admin_column' => true,
      'show_in_nav_menus' => true,
      'show_tagcloud' => true,
      'rewrite' => $rewrite,
  );
  
  // Finally register taxonomy
  register_taxonomy(FEEDWORDPRESSBYXML_CT_NAME, array(FEEDWORDPRESSBYXML_CPT_NAME), $args);
}

// Hook into the 'init' action
add_action('init', FEEDWORDPRESSBYXML_CT_NAME, 0);
