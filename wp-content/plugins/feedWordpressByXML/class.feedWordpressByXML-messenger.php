<?php

/*
 * Manage admin panel & options
 */

class feedWordpressByXML_Messenger {

  private $message = array();

  public function setMessage($msg, $type = "updated") {
    $this->message[] = array('type' => $type, 'msg' => $msg);
  }

  public function getAllMessage(){
    return $this->message;
  }

  public function getUpdated(){
    $notice = array();

    foreach( $this->message as $msg){
      if( $msg['type'] === 'updated'){
        $notice[] = $msg['msg'];
      }
    }

    return $notice;
  }

  public function getError(){
    $error = array();

    foreach( $this->message as $msg){
      if( $msg['type'] === 'error'){
        $error[] = $msg['msg'];
      }
    }

    return $error;
  }

  public function hasMessage(){
    return ! empty($this->message);
  }

  public function flushMessage(){
    $this->message = array();
  }

}
