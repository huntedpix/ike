<?php

/**
 * Plugin Name: feedWordpressByXML
 * Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
 * Description: Feed wordpress post by xml files
 * Version: 00.01.00
 * Author: Grégory Scheuren
 * Author URI: http://URI_Of_The_Plugin_Author
 * License: GPL2
 */

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
  echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
  exit(0);
}

define('FEEDWORDPRESSBYXML__PLUGIN_URL', plugin_dir_url(__FILE__));
define('FEEDWORDPRESSBYXML__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('FEEDWORDPRESSBYXML_CPT_NAME', 'jobs');
define('FEEDWORDPRESSBYXML_CT_NAME', 'jobs_taxonomy');

// Load plugin textdomain.
add_action( 'plugins_loaded', 'myplugin_load_textdomain' );
function myplugin_load_textdomain() {
  load_plugin_textdomain( 'feedWordpressByXML', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' ); 
}

// Create custom post type 'jobs' and custom taxonomy 'jobs_taxonomy'
require_once( FEEDWORDPRESSBYXML__PLUGIN_DIR . 'feedWordpressByXML-cpt-jobs.php' );

// Call necessary class
require_once( FEEDWORDPRESSBYXML__PLUGIN_DIR . 'class.feedWordpressByXML.php' );
require_once( FEEDWORDPRESSBYXML__PLUGIN_DIR . 'class.feedWordpressByXML-getter.php' );
require_once( FEEDWORDPRESSBYXML__PLUGIN_DIR . 'class.feedWordpressByXML-parser.php' );
require_once( FEEDWORDPRESSBYXML__PLUGIN_DIR . 'class.feedWordpressByXML-postHandler.php' );
require_once( FEEDWORDPRESSBYXML__PLUGIN_DIR . 'class.feedWordpressByXML-messenger.php' );

register_activation_hook(__FILE__, array('FeedWordpressByXML', 'plugin_activation'));
register_deactivation_hook(__FILE__, array('FeedWordpressByXML', 'plugin_deactivation'));

// Set as global to be available in all script file -> You have a better idea, i'm really interested to know it !
$GLOBALS['msg'] = new feedWordpressByXML_Messenger();

// Build admin
if( is_admin() ){
  require_once( FEEDWORDPRESSBYXML__PLUGIN_DIR . 'class.feedWordpressByXML-admin.php' );
  add_action('init', array('feedWordpressByXML_Admin', 'init'));
}

// Update
if( isset($_POST['feedWPByXML']) && $_POST['feedWPByXML'] === '1' && isset($_POST['update_requested']) && $_POST['update_requested'] === '1'){  
  add_action('init', array('FeedWordpressByXML', 'update'));
}

// Hook Cron
add_action('feedWordpressByXMLCron', array('FeedWordpressByXML', 'update'));