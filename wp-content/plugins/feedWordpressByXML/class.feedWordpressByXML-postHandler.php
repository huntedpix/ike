<?php

/*
 * Parse XML content
 */

class feedWordpressByXML_PostHandler {

  private $xml = null;
  private $currentUserId = 0;

  public function init($items) {

    $this->setUserRight();

    $this->xml = new feedWordpressByXML_Parser();

    // For now we delete all post and import new post
    $isPostDeleted = $this->deleteAllPosts();
    if( $isPostDeleted ){
      // All previous post is deleted, we can import new one
      foreach($items as $item) {

        // We want only post younger than one year
        $date = strtotime((string) $this->xml->getPublicationDate($item));
        $currentDate = time();
        $oneyear = 31556926;

        if(($currentDate - $date) <= $oneyear){
          $this->createPost($item);
        }
      }
    }else{
      throw new Exception(__("Update Jobs post Fail. Can't delete previous post", 'feedWordpressByXML'));
    }

    $this->restoreUserRight();

  }

  public function createPost($item) {

    // To set default languages
    global $polylang;

    // Prepare post
    $post = array(
      'post_content'   => $this->xml->getDescription($item), // The full text of the post.
      'post_title'     => $this->xml->getTitle($item), // The title of your post.
      'post_status'    => ($this->xml->getPublishedStatus($item) !== 1 ? 'draft' : 'publish'), // Default 'draft'.
      'post_type'      => FEEDWORDPRESSBYXML_CPT_NAME, // Default 'post'.
      'post_author'    => $this->xml->getAuthorID($item), //[ <user ID> ], // The user ID number of the author. Default is the current user ID.
      'ping_status'    => 'closed', // Pingbacks or trackbacks allowed. Default is the option 'default_ping_status'
      'post_excerpt'   => $this->xml->getExcerpt($item), // For all your post excerpt needs.
      'post_date'      => $this->xml->getPublicationDate($item), // The time post was made.
      'comment_status' => 'closed', // Default is the option 'default_comment_status', or 'closed'.
      'tax_input'      => array(FEEDWORDPRESSBYXML_CT_NAME => $this->xml->getCategories($item)) // [ array( <taxonomy> => <array | string> ) ] // For custom taxonomies. Default empty.
    );

    // Insert the post into the database and get id
    $postEN = wp_insert_post($post);
    $postFR = wp_insert_post($post);

    // Set default languages
    // Default languages for post are french -> At this time it seems that there are much post in FR than EN
    // To be able to show the page in fr and en we duplicate the post
    // TODO : Language autodectection
    $polylang->model->set_post_language($postEN, 'en');
    $polylang->model->set_post_language($postFR, 'fr');
    $polylang->model->save_translations('post', $postFR, array('en' => $postEN));

  }

  public function countPosts() {
    // WP_Query arguments
    $args = array (
      'post_type' => FEEDWORDPRESSBYXML_CPT_NAME,
      'posts_per_page' => '1', // Get only 1 item
    );

    // The Query
    $query = new WP_Query( $args );

    return $query->post_count;
  }

  public function deleteAllPosts() {
    global $wpdb;

    // First check if we have post
    if( !$this->countPosts() ){
      // No post so it's ok to import new one
      return true;
    }

    $deleteResults = $wpdb->delete($wpdb->posts, array('post_type' => FEEDWORDPRESSBYXML_CPT_NAME));

    if( !$deleteResults ){
      return false;
    }else{
      return true;
    }
  }

  private function setUserRight() {
    // Store real current id
    $this->currentUserId = get_current_user_id();

    // Cron task isn't allow to create term and post so we set current user as root
    $this->logUserById(1);
  }

  private function restoreUserRight() {
    // Restore user
    $this->logUserById($this->currentUserId);
  }

  private function logUserById($id){
    $user = get_user_by( 'id', $id );
    if( $user ) {
      wp_set_current_user( $id, $user->user_login );
      wp_set_auth_cookie( $id );
      do_action( 'wp_login', $user->user_login );
    }
  }



}
