<?php

/*
 * Manage admin panel & options
 */

class feedWordpressByXML_Admin {

  public static function init() {
    self::init_hooks();

    // Load translation
    load_plugin_textdomain( 'feedWordpressByXML' );
  }

  public function init_hooks() {
    add_action( 'admin_menu', array('feedWordpressByXML_Admin', 'admin_menu'), 5 );
  }

  public static function admin_menu(){
    // Add submenu to jobs custom post_type
    add_submenu_page( 'edit.php?post_type=jobs', __('Feed WP By XML Settings', 'fwpbxml'), __('Settings', 'fwpbxml'), 'edit_posts', 'feed-wp-by-xml-settings', array( 'feedWordpressByXML_Admin', 'displaySettingPage' ));
  }

  public static function checkIfAllow() {
    if (!current_user_can('edit_posts')) {
      wp_die(__('You do not have sufficient permissions to access this page.'));
    }
  }

  public static function getLastUpdate() {
    // First get current user
    global $current_user;
    get_currentuserinfo();

    $lastUpdate = get_option('feedWordpressByXML_last_update');
    $userLang = get_user_meta($current_user->id, 'user_lang', true);

    // Check if update has been already done once or not
    if( $lastUpdate === '' || !$lastUpdate ){
      return false;
    }else{
      if( $userLang === 'fr_FR' ){
        return date('H:i:s d-m-Y', $lastUpdate);
      }else{
        return date('H:i:s m-d-Y', $lastUpdate);
      }
    }
  }

  public function displaySettingPage() {
    self::checkIfAllow();

    $msg = $GLOBALS['msg'];

    $lastUpdate = self::getLastUpdate();

    $content =
            '<div class="wrap">'
              . '<h2>Feed WordPress By XML</h2>';

    if($lastUpdate !== false){
      $text = __('Last update', 'feedWordpressByXML');
      $content .= '<p>'. $text .' : '. $lastUpdate . '</p>';
    }

    if($msg->hasMessage()){
      foreach($msg->getAllMessage() as $m){

        $content .=
            '<div class="'.$m['type'].'">'
              . '<p>'.$m['msg'].'</p>'
          . '</div>';
      }

      // All messages are display
      $msg->flushMessage();
    }
    $buttonText = __('Update now !', 'feedWordpressByXML');

    $content .=
            '<p>Update jobs news</p>'
          . '<form method="post" action="?post_type=jobs&page=feed-wp-by-xml-settings">'
            . '<input type="hidden" name="feedWPByXML" value="1" />'
            . '<button value="1" name="update_requested" class="button">'. $buttonText .'</update>'
          . '</form>'
        . '</div>';

    echo $content;
  }

}
