<?php

/*
 * Get content from xml
 */

class feedWordpressByXML_Getter {

  public function getURLContent() {
    return 'http://cloud.ike.lu/apps/jobs/broadbean_ike/?action=get';
    //return 'http://www.lancelot-network.local/feedtest.xml';
  }

  public function getLogin() {
    // Hard coded for now
    return 'maintenance:devmaintenance12';
  }

  public function getXMLContent() {

    $ch = curl_init();

    $header = array( sprintf('Authorization: Basic %s', base64_encode($this->getLogin()) ) );

    curl_setopt($ch, CURLOPT_URL, $this->getURLContent());
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    $content = curl_exec($ch);

    curl_close($ch);

    return $content;
  }

}
