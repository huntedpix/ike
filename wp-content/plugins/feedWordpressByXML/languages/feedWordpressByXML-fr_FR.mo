��    &      L  5   |      P     Q     b     o     �     �  _   �  
      $        0     >     G     _  
   d     o     �     �  	   �     �     �     �     �     �          #     ?     Q     ]     }      �     �     �  
   �  1   �          !  ;   *     f  Q  k     �     �  $   �     	     0	  �   >	     �	  5   �	     
      
     .
     H
     M
     \
     {
     �
     �
     �
      �
     �
     �
                    !     <  *   N     y     �  
   �     �     �  O   �          ;  A   G     �     $                                                                           
            	             !          %   "         #                                 &                  Add New Category Add New Jobs Add or remove Categories All Categories All Jobs Can't get jobs content. Please try later and if the problem persist contact your administrator. Categories Choose from the most used Categories Edit Category Edit Job Feed WP By XML Settings Jobs Jobs offer Jobs posts has been updated Last update New Category Name Not Found Not Jobs found Not Jobs found in Trash Parent Category Parent Category: Parent Job: Post Type General NameJobs Post Type Singular NameJob Search Categories Search Jobs Separate Categories with commas Settings Taxonomy General NameCategories Taxonomy Singular NameCategory Update Category Update Job Update Jobs post Fail. Can't delete previous post Update now ! View Job You do not have sufficient permissions to access this page. jobs Project-Id-Version: feedWordpressByXML
POT-Creation-Date: 2014-07-11 23:06+0100
PO-Revision-Date: 2014-07-11 23:22+0100
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.6
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: .
 Ajouter une nouvelle catégorie Ajouter un nouveau job Ajouter ou supprimer des catégories Toutes les catégories Tous les jobs Impossible de récupérer les jobs. Veuillez s'il vous plaît essayer plus tard. Si le problème persiste, contacter votre administrateur. Catégories Choisir à partir des catégories les plus utilisées Editer la catégorie Editer le job Feed WP By XML Paramètre Jobs Annonce de job Les jobs ont été mis à jour Dernière mise à jour Nouvelle catégorie Pas trouvé Aucun job trouvé Aucun job trouvé à la poubelle Catégorie parente Catégorie parente: Job parent: Jobs Job Rechercher des catégories Rechercher un job Séparer les catégories avec des virgules Paramètres Catégories Catégorie Mettre à jour la catégorie Mettre à jour le job La mise à jour des Jobs a échoué. Impossible d'effacer les jobs précédent. Mettre à jour maintenant ! Voir le job Vous n'avez pas les droits suffisant pour accéder à cette page. jobs 