<?php

// Register Custom Post Type
function values_post_type() {

	$labels = array(
		'name'                  => _x( 'Values', 'Post Type General Name', 'ike_I18N' ),
		'singular_name'         => _x( 'Value', 'Post Type Singular Name', 'ike_I18N' ),
		'menu_name'             => __( 'Values', 'ike_I18N' ),
		'name_admin_bar'        => __( 'Value', 'ike_I18N' ),
		'archives'              => __( 'Value Archives', 'ike_I18N' ),
		'attributes'            => __( 'Value Attributes', 'ike_I18N' ),
		'parent_item_colon'     => __( 'Parent Value:', 'ike_I18N' ),
		'all_items'             => __( 'All Values', 'ike_I18N' ),
		'add_new_item'          => __( 'Add New Value', 'ike_I18N' ),
		'add_new'               => __( 'Add New', 'ike_I18N' ),
		'new_item'              => __( 'New Value', 'ike_I18N' ),
		'edit_item'             => __( 'Edit Value', 'ike_I18N' ),
		'update_item'           => __( 'Update Value', 'ike_I18N' ),
		'view_item'             => __( 'View Value', 'ike_I18N' ),
		'view_items'            => __( 'View Values', 'ike_I18N' ),
		'search_items'          => __( 'Search Value', 'ike_I18N' ),
		'not_found'             => __( 'Not found', 'ike_I18N' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ike_I18N' ),
		'featured_image'        => __( 'Featured Image', 'ike_I18N' ),
		'set_featured_image'    => __( 'Set featured image', 'ike_I18N' ),
		'remove_featured_image' => __( 'Remove featured image', 'ike_I18N' ),
		'use_featured_image'    => __( 'Use as featured image', 'ike_I18N' ),
		'insert_into_item'      => __( 'Insert into Value', 'ike_I18N' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Value', 'ike_I18N' ),
		'items_list'            => __( 'Values list', 'ike_I18N' ),
		'items_list_navigation' => __( 'Values list navigation', 'ike_I18N' ),
		'filter_items_list'     => __( 'Filter Values list', 'ike_I18N' ),
	);
	$args = array(
		'label'                 => __( 'Value', 'ike_I18N' ),
		'description'           => __( 'iKe references', 'ike_I18N' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-heart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'values', $args );

}
add_action( 'init', 'values_post_type', 0 );
