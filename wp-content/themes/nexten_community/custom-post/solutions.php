<?php

// Register Custom Post Type
function solutions_post_type() {

	$labels = array(
		'name'                  => _x( 'Solutions', 'Post Type General Name', 'ike_I18N' ),
		'singular_name'         => _x( 'Solution', 'Post Type Singular Name', 'ike_I18N' ),
		'menu_name'             => __( 'Solutions', 'ike_I18N' ),
		'name_admin_bar'        => __( 'Solution', 'ike_I18N' ),
		'archives'              => __( 'Solution Archives', 'ike_I18N' ),
		'attributes'            => __( 'Solution Attributes', 'ike_I18N' ),
		'parent_item_colon'     => __( 'Parent Solution:', 'ike_I18N' ),
		'all_items'             => __( 'All Solutions', 'ike_I18N' ),
		'add_new_item'          => __( 'Add New Solution', 'ike_I18N' ),
		'add_new'               => __( 'Add New', 'ike_I18N' ),
		'new_item'              => __( 'New Solution', 'ike_I18N' ),
		'edit_item'             => __( 'Edit Solution', 'ike_I18N' ),
		'update_item'           => __( 'Update Solution', 'ike_I18N' ),
		'view_item'             => __( 'View Solution', 'ike_I18N' ),
		'view_items'            => __( 'View Solutions', 'ike_I18N' ),
		'search_items'          => __( 'Search Solution', 'ike_I18N' ),
		'not_found'             => __( 'Not found', 'ike_I18N' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ike_I18N' ),
		'featured_image'        => __( 'Featured Image', 'ike_I18N' ),
		'set_featured_image'    => __( 'Set featured image', 'ike_I18N' ),
		'remove_featured_image' => __( 'Remove featured image', 'ike_I18N' ),
		'use_featured_image'    => __( 'Use as featured image', 'ike_I18N' ),
		'insert_into_item'      => __( 'Insert into Solution', 'ike_I18N' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Solution', 'ike_I18N' ),
		'items_list'            => __( 'Solutions list', 'ike_I18N' ),
		'items_list_navigation' => __( 'Solutions list navigation', 'ike_I18N' ),
		'filter_items_list'     => __( 'Filter Solutions list', 'ike_I18N' ),
	);
	$args = array(
		'label'                 => __( 'Solution', 'ike_I18N' ),
		'description'           => __( 'iKe references', 'ike_I18N' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-lightbulb',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'solutions', $args );

}
add_action( 'init', 'solutions_post_type', 0 );
