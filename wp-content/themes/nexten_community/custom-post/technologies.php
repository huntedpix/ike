<?php

// Register Custom Post Type
function technologies_post_type() {

	$labels = array(
		'name'                  => _x( 'Technologies', 'Post Type General Name', 'ike_I18N' ),
		'singular_name'         => _x( 'Technology', 'Post Type Singular Name', 'ike_I18N' ),
		'menu_name'             => __( 'Technologies', 'ike_I18N' ),
		'name_admin_bar'        => __( 'Technology', 'ike_I18N' ),
		'archives'              => __( 'Technology Archives', 'ike_I18N' ),
		'attributes'            => __( 'Technology Attributes', 'ike_I18N' ),
		'parent_item_colon'     => __( 'Parent Technology:', 'ike_I18N' ),
		'all_items'             => __( 'All Technologies', 'ike_I18N' ),
		'add_new_item'          => __( 'Add New Technology', 'ike_I18N' ),
		'add_new'               => __( 'Add New', 'ike_I18N' ),
		'new_item'              => __( 'New Technology', 'ike_I18N' ),
		'edit_item'             => __( 'Edit Technology', 'ike_I18N' ),
		'update_item'           => __( 'Update Technology', 'ike_I18N' ),
		'view_item'             => __( 'View Technology', 'ike_I18N' ),
		'view_items'            => __( 'View Technologies', 'ike_I18N' ),
		'search_items'          => __( 'Search Technology', 'ike_I18N' ),
		'not_found'             => __( 'Not found', 'ike_I18N' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ike_I18N' ),
		'featured_image'        => __( 'Featured Image', 'ike_I18N' ),
		'set_featured_image'    => __( 'Set featured image', 'ike_I18N' ),
		'remove_featured_image' => __( 'Remove featured image', 'ike_I18N' ),
		'use_featured_image'    => __( 'Use as featured image', 'ike_I18N' ),
		'insert_into_item'      => __( 'Insert into Technology', 'ike_I18N' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Technology', 'ike_I18N' ),
		'items_list'            => __( 'Technologies list', 'ike_I18N' ),
		'items_list_navigation' => __( 'Technologies list navigation', 'ike_I18N' ),
		'filter_items_list'     => __( 'Filter Technologies list', 'ike_I18N' ),
	);
	$args = array(
		'label'                 => __( 'Technology', 'ike_I18N' ),
		'description'           => __( 'iKe references', 'ike_I18N' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-hammer',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'technologies', $args );

}
add_action( 'init', 'technologies_post_type', 0 );
