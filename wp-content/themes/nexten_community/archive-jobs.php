<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

	<?php if ( have_posts() ) : ?>
		<header class="page-header">
      <h1 class="page-title">
        <?php echo __('Our job offers', ike_I18N); ?>
      </h1>
		</header><!-- .page-header -->
	<?php
    endif;

		if ( have_posts() ) : ?>
    <ul class="jobs-list">
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
      $location = get_field('work-place', get_the_ID());
      ?>
      <li class="jobs-item">
        <a href="<?php the_permalink(); ?>">
          <h3 class="jobs-title"><?php the_title(); ?></h3>
          | <span class="jobs-date"><?php echo get_the_date(); ?></span>
          <?php if ($location): ?>
          | <span class="jobs-location"><?php echo $location; ?></span>
          <?php endif; ?>
        </a>
      </li>
<?php
			endwhile;

			the_posts_pagination( array(
				'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'ike_I18N' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'ike_I18N' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'ike_I18N' ) . ' </span>',
			) );

		else :

			get_template_part( 'template-parts/post/content', 'none' );

		endif; ?>
</ul>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
