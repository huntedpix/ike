<section id="ike-unplugged">
  <h1 class="home-section-title">Nexten Community</h1>
  <div class="instagram-pictures">
      <?php
        echo do_shortcode( '[instagram-feed]' );
      ?>
  </div>

  <?php if ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) : ?>
    <a href="#jobs" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'ike_I18N' ); ?></span></a>
  <?php endif; ?>
</section>
