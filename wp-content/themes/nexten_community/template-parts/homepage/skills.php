<?php

  $defaultLanguagePostID = 60;
  $postID = pll_get_post($defaultLanguagePostID, pll_current_language());

  // If there are no translation, pll_get_post return false.
  if ($postID === false) {
    $postID = $defaultLanguagePostID;
  }

  $post = get_post($postID);

  $args = array(
    'post_type' => 'skills',
    'order' => 'ASC',
    'posts_per_page' => -1,
    'lang' => pll_current_language()
  );
  $loop = new WP_Query($args);
?>

<section id="skills">
  <h1 class="home-section-title"><?php echo $post->post_title; ?></h1>
  <div class="skills-content">
    <?php echo $post->post_content; ?>
  </div>

  <?php
  // We don't want skills list for the moment, that's why there is a false
  // into the condition statement.
  if (false && $loop->have_posts()) :
  ?>
  <ul class="skills-list">
  <?php
    while ($loop->have_posts()) :
      $loop->the_post();
  ?>
    <li class="skills-item">
      <?php the_post_thumbnail(); ?>
      <h3 class="skills-title"><?php the_title(); ?></h3>
      <div class="skills-description"><?php the_content(); ?></div>
    </li>
    <?php
    endwhile;
    ?>
  </ul>
  <?php
  endif;
  get_template_part('template-parts/homepage/technologies');
  // get_template_part('template-parts/homepage/solutions');
  ?>

  <?php
  if ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) : ?>
    <a href="#ike-unplugged" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'ike_I18N' ); ?></span></a>
  <?php endif; ?>
</section>
