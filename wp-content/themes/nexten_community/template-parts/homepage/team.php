<?php
  $args = array(
    'role' => 'editor',
  );

  $teams = get_users($args);
?>
<div class="team-members">
  <?php
    foreach ($teams as $member):
      $user_id = 'user_' . $member->id;
  ?>
    <div class="team-member">
        <div class="team-member-pic">
          <img src="<?php echo get_field('photo', $user_id); ?>" alt=""/>
        </div>
        <div class="team-member-profile">
          <h4 class="team-member-name"><?php echo $member->display_name; ?>
            <a href="<?php echo get_field('linkedin', $user_id); ?>" target="_blank" class="team-member-linkedin">
              <span class="icon">
                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30 30" xml:space="preserve">
                    <g>
                      <rect x="0.4" y="10" class="st0" width="6.4" height="19.3"></rect>
                      <path class="st0" d="M3.6,7.3L3.6,7.3C1.4,7.3,0,5.9,0,4c0-1.9,1.4-3.3,3.6-3.3c2.2,0,3.6,1.4,3.6,3.3C7.2,5.9,5.8,7.3,3.6,7.3z"></path>
                      <path class="st0" d="M30,29.3h-6.4V19c0-2.6-0.9-4.4-3.3-4.4c-1.8,0-2.8,1.2-3.3,2.4c-0.2,0.4-0.2,1-0.2,1.6v10.8h-6.4c0,0,0.1-17.5,0-19.3h6.4v2.7c0.9-1.3,2.4-3.2,5.8-3.2c4.2,0,7.4,2.8,7.4,8.7V29.3z"></path>
                    </g>
                  </svg>
              </span>
            </a>
          </h4>
          <span class="team-member-role"><?php echo get_field('role', $user_id); ?></span>
          <a href="tel:<?php echo get_field('telephone', $user_id); ?>" class="team-member-phone"><?php echo get_field('telephone', $user_id); ?></a>
        </div>
    </div>
  <?php endforeach; ?>
</div>
