<?php
  $args = array(
    'post_type' => 'jobs',
    'order' => 'DESC',
    'posts_per_page' => 10,
    'lang' => pll_current_language()
  );
  $loop = new WP_Query($args);
?>

<section id="jobs">
  <h1 class="home-section-title"><?php echo __('Jobs openings', ike_I18N); ?></h1>
  <div class="jobs-content">
    <p><?php echo __('We are looking for motivated people who share our mindset and values. If you are looking for a fast growing and innovative company, to work on great project, join us!', ike_I18N); ?></p>
  <?php
  if ($loop->have_posts()) :
  ?>
  <ul class="jobs-list">
  <?php
    while ($loop->have_posts()) :
      $loop->the_post();
      $location = get_field('work-place', get_the_ID());
  ?>
    <li class="jobs-item">
      <a href="<?php the_permalink(); ?>">
        <h3 class="jobs-title"><?php the_title(); ?></h3>
        | <span class="jobs-date"><?php echo get_the_date(); ?></span>
        <?php if ($location): ?>
        | <span class="jobs-location"><?php echo $location; ?></span>
        <?php endif; ?>
      </a>
    </li>
    <?php
    endwhile;
    ?>
  </ul>
  <?php
  endif;
  ?>
  <a href="<?php echo site_url('jobs'); ?>" class="more-jobs"><?php _e('See more job offers', 'ike_I18N'); ?></a>
  <?php
    if ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) :
      $defaultLanguagePostID = 706;
      $postID = pll_get_post($defaultLanguagePostID, pll_current_language());

      // If there are no translation, pll_get_post return false.
      if ($postID === false) {
        $postID = $defaultLanguagePostID;
      }
      $post = get_post($postID);
  ?>
    <div class="no-jobs">
      <?php echo apply_filters('the_content',$post->post_content); ?>
    </div>
    <?php endif; ?>
  </div>
</section>
