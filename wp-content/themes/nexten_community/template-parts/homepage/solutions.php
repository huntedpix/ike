<?php

$args = array(
  'post_type' => 'solutions',
  'order' => 'ASC',
  'posts_per_page' => -1,
);
$loop = new WP_Query($args);
if ($loop->have_posts()) :
?>
  <div id="solutions">
    <h3>Solutions</h3>
    <ul class="showcase-list">
<?php
  while ($loop->have_posts()) :
    $loop->the_post();
?>
      <li>
        <figure class="solution">
          <?php the_post_thumbnail(); ?>
          <figcaption><?php the_title(); ?></figcaption>
        </figure>
      </li>
<?php
  endwhile;
endif;
?>
</div>
