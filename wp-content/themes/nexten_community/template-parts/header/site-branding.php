<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-branding">
	<div class="wrap">
		<div class="site-branding-text">
			<?php if ( is_front_page() ) : ?>
				<h1 class="site-title">
          <?php /*
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
          </a>
          */
          ?>
            <img src="<?php echo get_template_directory_uri() . '/assets/images/logo-nexten-community.svg' ?>" alt="<?php bloginfo( 'name' ); ?>" class="site-logo" width="300" height="300">
        </h1>
			<?php else : ?>
				<p class="site-title">
          <img src="<?php echo get_template_directory_uri() . '/assets/images/logo-nexten-community.svg' ?>" alt="<?php bloginfo( 'name' ); ?>" class="site-logo" width="300" height="300">
        </p>
			<?php endif; ?>
		</div><!-- .site-branding-text -->


	</div><!-- .wrap -->
</div><!-- .site-branding -->

<?php if ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) : ?>
  <a href="#primary" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'ike_I18N' ); ?></span></a>
<?php endif; ?>
