<?php
/**
 * Displays top navigation on every page except home page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
$url = site_url().(pll_current_language() === 'en' ? '/en/home/' : '');
?>
<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'ike_I18N' ); ?>">
	<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
		<?php
		echo twentyseventeen_get_svg( array( 'icon' => 'bars' ) );
		echo twentyseventeen_get_svg( array( 'icon' => 'close' ) );
		_e( 'Menu', 'ike_I18N' );
		?>
	</button>
  <div class="menu-menu-principal-container">
    <ul id="top-menu" class="menu">
      <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-69">
        <a href="<?php echo $url; ?>#identity"><?php echo __('About us', 'ike_I18N'); ?></a>
        <ul  class="sub-menu">
          <li id="menu-item-72" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72"><a href="<?php echo $url; ?>#identity"><?php echo __('Our compagny', 'ike_I18N'); ?></a></li>
          <li id="menu-item-71" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71"><a href="<?php echo $url; ?>#values"><?php echo __('Our values', 'ike_I18N'); ?></a></li>
          <li id="menu-item-70" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70"><a href="<?php echo $url; ?>#references"><?php echo __('Our references', 'ike_I18N'); ?></a></li>
        </ul>
      </li>
      <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-66">
        <a href="<?php echo $url; ?>#mission"><?php echo __('Our expertise', 'ike_I18N'); ?></a>
        <ul  class="sub-menu">
          <li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68"><a href="<?php echo $url; ?>#mission"><?php echo __('Our mission', 'ike_I18N'); ?></a></li>
          <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67"><a href="<?php echo $url; ?>#skills"><?php echo __('Our expertise', 'ike_I18N'); ?></a></li>
        </ul>
      </li>
      <li id="menu-item-361" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-361"><a href="<?php echo $url; ?>#page"><?php echo __('Home', 'ike_I18N'); ?></a></li>
      <li id="menu-item-360" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-360"><a href="<?php echo $url; ?>#jobs"><?php echo __('Careers', 'ike_I18N'); ?></a></li>
      <li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a href="<?php echo $url; ?>#contact"><?php echo __('Contact us', 'ike_I18N'); ?></a></li>
    </ul>
    <ul class="nav-langswitch">
      <?php pll_the_languages();?>
    </ul>
  </div>
</nav><!-- #site-navigation -->
