<?php
/*
  Template Name: Homepage
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php
    get_template_part('template-parts/homepage/identity');
    get_template_part('template-parts/homepage/values');
    get_template_part('template-parts/homepage/clients');
    get_template_part('template-parts/homepage/mission');
    get_template_part('template-parts/homepage/skills');
    get_template_part('template-parts/homepage/ike-unplugged');
    get_template_part('template-parts/homepage/jobs');
    get_template_part('template-parts/homepage/contact');
  ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
