<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/post/content', 'jobs' );

				// the_post_navigation( array(
				// 	'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'ike_I18N' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'ike_I18N' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
				// 	'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'ike_I18N' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'ike_I18N' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
				// ) );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
  <div id="secondary">
    <?php get_template_part('template-parts/jobs/sidebar')?>
  </div>
</div><!-- .wrap -->
<div id="contact">
  <div class="contact-jobs">
  <?php
    if (pll_current_language() === 'fr') {
      echo '<h2>Nous contacter</h2>';
      echo do_shortcode( '[contact-form-7 id="506"]' );
    } else {
      echo '<h2>Contact us</h2>';
      echo do_shortcode( '[contact-form-7 id="507"]' );
    }
  ?>
  </div>
</div>

<?php get_footer();
