<?php

/*
 * Add custom post type to wordpress.
 * Here is the jobs custom post type creation and settings
 * Jobs custom post type will be used to display jobs from braodbean feed
 */

// Register Jobs post
function register_cpt_jobs() {

  // Set labels
  $labels = array(
      'name' => _x('Jobs', 'Post Type General Name', 'ike_I18N'),
      'singular_name' => _x('Job', 'Post Type Singular Name', 'ike_I18N'),
      'menu_name' => __('Jobs', 'ike_I18N'),
      'parent_item_colon' => __('Parent Job:', 'ike_I18N'),
      'all_items' => __('All Jobs', 'ike_I18N'),
      'view_item' => __('View Job', 'ike_I18N'),
      'add_new_item' => __('Add New Jobs', 'ike_I18N'),
      'add_new' => __('Add New Jobs', 'ike_I18N'),
      'edit_item' => __('Edit Job', 'ike_I18N'),
      'update_item' => __('Update Job', 'ike_I18N'),
      'search_items' => __('Search Jobs', 'ike_I18N'),
      'not_found' => __('Not Jobs found', 'ike_I18N'),
      'not_found_in_trash' => __('Not Jobs found in Trash', 'ike_I18N'),
  );

  // Set Rewrite settings
  $rewrite = array(
      'slug' => 'jobs',
      'with_front' => true,
      'pages' => true,
      'feeds' => true,
  );

  // Than set post_type settings add labels and rewrite settings
  $args = array(
      'label' => __('jobs', 'ike_I18N'),
      'description' => __('Jobs offer', 'ike_I18N'),
      'labels' => $labels,
      'supports' => array('title', 'editor', 'excerpt', 'author',),
      'taxonomies' => array('jobs_taxonomy'),
      'hierarchical' => false,
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => false,
      'show_in_admin_bar' => false,
      'menu_position' => 5,
      'can_export' => true,
      'has_archive' => true,
      'exclude_from_search' => false,
      'publicly_queryable' => true,
      'rewrite' => $rewrite,
      'capability_type' => 'post',
  );

  // Finaly register post type
  register_post_type('jobs', $args);
}

// Hook into the 'init' action
add_action('init', 'register_cpt_jobs', 0);

// Register Custom Jobs Taxonomy
function jobs_taxonomy() {

  // Set labels
  $labels = array(
      'name' => _x('Categories', 'Taxonomy General Name', 'ike_I18N'),
      'singular_name' => _x('Category', 'Taxonomy Singular Name', 'ike_I18N'),
      'menu_name' => __('Categories', 'ike_I18N'),
      'all_items' => __('All Categories', 'ike_I18N'),
      'parent_item' => __('Parent Category', 'ike_I18N'),
      'parent_item_colon' => __('Parent Category:', 'ike_I18N'),
      'new_item_name' => __('New Category Name', 'ike_I18N'),
      'add_new_item' => __('Add New Category', 'ike_I18N'),
      'edit_item' => __('Edit Category', 'ike_I18N'),
      'update_item' => __('Update Category', 'ike_I18N'),
      'separate_items_with_commas' => __('Separate Categories with commas', 'ike_I18N'),
      'search_items' => __('Search Categories', 'ike_I18N'),
      'add_or_remove_items' => __('Add or remove Categories', 'ike_I18N'),
      'choose_from_most_used' => __('Choose from the most used Categories', 'ike_I18N'),
      'not_found' => __('Not Found', 'ike_I18N'),
  );

  // Set Rewrite settings
  $rewrite = array(
      'slug' => 'filter',
      'with_front' => true,
      'hierarchical' => true,
  );

  // Than set taxonomy settings add labels and rewrite settings
  $args = array(
      'labels' => $labels,
      'hierarchical' => true,
      'public' => true,
      'show_ui' => true,
      'show_admin_column' => true,
      'show_in_nav_menus' => true,
      'show_tagcloud' => true,
      'rewrite' => $rewrite,
  );

  // Finally register taxonomy
  register_taxonomy('jobs_taxonomy', array('jobs'), $args);
}

// Hook into the 'init' action
add_action('init', 'jobs_taxonomy', 0);
