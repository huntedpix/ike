<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	© iKe. All rights reserved.
</div><!-- .site-info -->
<address class="footer-address">
  iKe, 153-155 Rue du Kiem - Entrée B, L-8030 Strassen, Grand-Duché de Luxembourg
  <span>Tél.: +352 20 40 22 40</span>
  <span>Fax: +352 20 40 22 41</span>
</address>
