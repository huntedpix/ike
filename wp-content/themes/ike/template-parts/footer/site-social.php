<div class="social">
    <ul class="social-list">
        <li>
            <a href="https://twitter.com/iKe_Luxembourg" target="_blank" id="twitter">
                <svg version="1.1" id="twitter-icon" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30 30" xml:space="preserve" width="30px" height="30px">
                    <path class="st0" d="M30,5.7c-1.1,0.5-2.3,0.8-3.5,1c1.3-0.8,2.2-2,2.7-3.4C28,4,26.7,4.5,25.3,4.8c-1.1-1.2-2.7-1.9-4.5-1.9c-3.4,0-6.2,2.8-6.2,6.2c0,0.5,0.1,1,0.2,1.4C9.7,10.1,5.1,7.7,2.1,3.9C1.6,4.8,1.3,5.9,1.3,7c0,2.1,1.1,4,2.7,5.1c-1,0-2-0.3-2.8-0.8c0,0,0,0.1,0,0.1c0,3,2.1,5.5,4.9,6c-0.5,0.1-1.1,0.2-1.6,0.2c-0.4,0-0.8,0-1.2-0.1c0.8,2.4,3.1,4.2,5.7,4.3c-2.1,1.7-4.8,2.6-7.6,2.6c-0.5,0-1,0-1.5-0.1c2.7,1.7,6,2.8,9.4,2.8c11.3,0,17.5-9.4,17.5-17.5c0-0.3,0-0.5,0-0.8C28.1,8,29.2,6.9,30,5.7z"></path>
                </svg>
            </a>
        </li>
        <li>
            <a href="https://www.facebook.com/pages/IKE-Consulting/342686399250739" target="_blank" id="facebook">
                <svg version="1.1" id="facebook-icon" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30 30" xml:space="preserve" width="30px" height="30px">
                    <path class="st0" d="M22,9.7h-4.7V6.6c0-1.2,0.8-1.4,1.3-1.4c0.5,0,3.4,0,3.4,0V0l-4.6,0C12.2,0,11,3.8,11,6.3v3.4H8V15h3c0,6.8,0,15,0,15h6.2c0,0,0-8.3,0-15h4.2L22,9.7z"></path>
                </svg>
            </a>
        </li>
        <li>
            <a href="https://www.linkedin.com/company/ike-consulting" target="_blank" id="linkedin">
                <svg version="1.1" id="linkedin-icon" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30 30" xml:space="preserve" width="30px" height="30px">
                    <g>
                        <rect x="0.4" y="10" class="st0" width="6.4" height="19.3"></rect>
                        <path class="st0" d="M3.6,7.3L3.6,7.3C1.4,7.3,0,5.9,0,4c0-1.9,1.4-3.3,3.6-3.3c2.2,0,3.6,1.4,3.6,3.3C7.2,5.9,5.8,7.3,3.6,7.3z"></path>
                        <path class="st0" d="M30,29.3h-6.4V19c0-2.6-0.9-4.4-3.3-4.4c-1.8,0-2.8,1.2-3.3,2.4c-0.2,0.4-0.2,1-0.2,1.6v10.8h-6.4c0,0,0.1-17.5,0-19.3h6.4v2.7c0.9-1.3,2.4-3.2,5.8-3.2c4.2,0,7.4,2.8,7.4,8.7V29.3z"></path>
                    </g>
                </svg>
            </a>
        </li>
    </ul>
</div>
