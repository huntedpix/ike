<?php

  $defaultLanguagePostID = 9;
  $postID = pll_get_post($defaultLanguagePostID, pll_current_language());

  // If there are no translation, pll_get_post return false.
  if ($postID === false) {
    $postID = $defaultLanguagePostID;
  }

  $post = get_post($postID);

  $args = array(
    'post_type' => 'values',
    'order' => 'ASC',
    'posts_per_page' => -1,
    'lang' => pll_current_language()
  );
  $loop = new WP_Query($args);
?>

<section id="values">
  <h1 class="home-section-title"><?php echo $post->post_title; ?></h1>
  <?php echo $post->post_content; ?>

  <?php
  if ($loop->have_posts()) :
  ?>
  <ul class="values-list">
  <?php
    while ($loop->have_posts()) :
      $loop->the_post();
  ?>
    <li class="value-item">
      <?php the_post_thumbnail(); ?>
      <h3 class="value-title"><?php the_title(); ?></h3>
      <div class="value-description"><?php the_content(); ?></div>
    </li>
    <?php
    endwhile;
    ?>
  </ul>
  <?php
  endif;
  if ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) : ?>
    <a href="#references" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'ike_I18N' ); ?></span></a>
  <?php endif; ?>
</section>
