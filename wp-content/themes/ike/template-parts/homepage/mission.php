<?php

  $defaultLanguagePostID = 58;
  $postID = pll_get_post($defaultLanguagePostID, pll_current_language());

  // If there are no translation, pll_get_post return false.
  if ($postID === false) {
    $postID = $defaultLanguagePostID;
  }

  $post = get_post($postID);
?>

<section id="mission">
  <h1 class="home-section-title"><?php echo $post->post_title; ?></h1>
  <div class="mission-content"><?php echo $post->post_content; ?></div>

  <?php
  if ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) : ?>
    <a href="#skills" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'ike_I18N' ); ?></span></a>
  <?php endif; ?>
</section>
