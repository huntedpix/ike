<?php

  $defaultLanguagePostID = 62;
  $postID = pll_get_post($defaultLanguagePostID, pll_current_language());

  // If there are no translation, pll_get_post return false.
  if ($postID === false) {
    $postID = $defaultLanguagePostID;
  }

  $post = get_post($postID);
?>

<section id="contact">
  <h1 class="home-section-title"><?php echo $post->post_title; ?></h1>
  <div class="wrapper">
    <div class="contact-content"><?php echo $post->post_content; ?></div>
    <div class="contact-form">
      <?php
        if (pll_current_language() === 'fr') {
          echo do_shortcode( '[contact-form-7 id="454" title="French contact form"]' );
        } else {
          echo do_shortcode( '[contact-form-7 id="505" title="English contact form"]' );
        }
      ?>
    </div>
  </div>
  <?php get_template_part('template-parts/homepage/team'); ?>
</section>
<section id="map" class="content">
    <a class="map-cover" href="#"></a>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5170.1057693547855!2d6.087856896356367!3d49.61560764207878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47954eb26de26c71%3A0xef20310d218fcbb1!2s155+Rue+du+Kiem%2C+8030+Strassen!5e0!3m2!1sfr!2slu!4v1468244670448" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
