<?php

  $defaultLanguagePostID = 53;
  $postID = pll_get_post($defaultLanguagePostID, pll_current_language());

  // If there are no translation, pll_get_post return false.
  if ($postID === false) {
    $postID = $defaultLanguagePostID;
  }

  $post = get_post($postID);

  $args = array(
    'post_type' => 'clients',
    'order' => 'ASC',
    'posts_per_page' => -1,
  );
  $loop = new WP_Query($args);
  if ($loop->have_posts()) :
?>
  <section id="references">
    <div class="references-content">
      <h1 class="home-section-title"><?php echo $post->post_title; ?></h1>
      <?php echo $post->post_content; ?>
    </div>
    <ul class="showcase-list">
<?php
  while ($loop->have_posts()) :
    $loop->the_post();
?>
      <li>
        <figure class="ref">
          <?php the_post_thumbnail(); ?>
          <figcaption><?php the_title(); ?></figcaption>
        </figure>
      </li>
<?php
  endwhile;
endif;
?>
  </ul>
  <?php if ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) : ?>
    <a href="#mission" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'ike_I18N' ); ?></span></a>
  <?php endif; ?>
</section>
