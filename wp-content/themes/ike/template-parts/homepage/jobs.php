<?php
  $args = array(
    'post_type' => 'jobs',
    'order' => 'DESC',
    'posts_per_page' => 10,
    'lang' => pll_current_language()
  );
  $loop = new WP_Query($args);
?>

<section id="jobs">
  <h1 class="home-section-title"><?php echo __('We are hiring', ike_I18N); ?></h1>
  <div class="jobs-content">
  <?php
  if ($loop->have_posts()) :
  ?>
  <ul class="jobs-list">
  <?php
    while ($loop->have_posts()) :
      $loop->the_post();
      $location = get_field('work-place', get_the_ID());
  ?>
    <li class="jobs-item">
      <a href="<?php the_permalink(); ?>">
        <h3 class="jobs-title"><?php the_title(); ?></h3>
        | <span class="jobs-date"><?php echo get_the_date(); ?></span>
        <?php if ($location): ?>
        | <span class="jobs-location"><?php echo $location; ?></span>
        <?php endif; ?>
      </a>
    </li>
    <?php
    endwhile;
    ?>
  </ul>
  <?php
  endif;
  ?>
  <a href="<?php echo site_url('jobs'); ?>" class="more-jobs"><?php _e('See more job offers', 'ike_I18N'); ?></a>
  <?php
    if ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) : ?>
    <div class="no-jobs">
      <p><?php _e('No job matches your profile? Send us your CV!', 'ike_I18N'); ?></p>
      <a href="#contact" class="menu-scroll-down menu-scroll-down---button">
        <?php _e( 'Submit a spontaneous application', 'ike_I18N' ); ?>
      </a>
    </div>
    <?php endif; ?>
  </div>
</section>
