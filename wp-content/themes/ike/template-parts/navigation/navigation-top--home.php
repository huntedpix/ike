<?php
/**
 * Displays top navigation on homepage
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'ike_I18N' ); ?>">
	<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
		<?php
		echo twentyseventeen_get_svg( array( 'icon' => 'bars' ) );
		echo twentyseventeen_get_svg( array( 'icon' => 'close' ) );
		_e( 'Menu', 'ike_I18N' );
		?>
	</button>
  <div class="menu-menu-principal-container">
    <ul id="top-menu" class="menu">
      <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-69">
        <a href="#identity" class="js-menu-scroll-down"><?php echo __('About us', 'ike_I18N'); ?></a>
        <ul  class="sub-menu">
          <li id="menu-item-72" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72"><a href="#identity" class="js-menu-scroll-down"><?php echo __('Our compagny', 'ike_I18N'); ?></a></li>
          <li id="menu-item-71" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71"><a href="#values" class="js-menu-scroll-down"><?php echo __('Our values', 'ike_I18N'); ?></a></li>
          <li id="menu-item-70" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70"><a href="#references" class="js-menu-scroll-down"><?php echo __('Our references', 'ike_I18N'); ?></a></li>
        </ul>
      </li>
      <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-66">
        <a href="#mission" class="js-menu-scroll-down"><?php echo __('Our expertise', 'ike_I18N'); ?></a>
        <ul  class="sub-menu">
          <li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68"><a href="#mission" class="js-menu-scroll-down"><?php echo __('Our mission', 'ike_I18N'); ?></a></li>
          <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67"><a href="#skills" class="js-menu-scroll-down"><?php echo __('Our expertise', 'ike_I18N'); ?></a></li>
        </ul>
      </li>
      <li id="menu-item-361" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-361"><a href="#page" class="js-menu-scroll-down"><?php echo __('Home', 'ike_I18N'); ?></a></li>
      <li id="menu-item-360" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-360"><a href="#jobs" class="js-menu-scroll-down"><?php echo __('Careers', 'ike_I18N'); ?></a></li>
      <li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a href="#contact" class="js-menu-scroll-down"><?php echo __('Contact us', 'ike_I18N'); ?></a></li>
    </ul>
    <ul class="nav-langswitch">
      <?php pll_the_languages();?>
    </ul>
  </div>
</nav><!-- #site-navigation -->
