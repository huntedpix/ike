<?php
  $args = array(
    'post_type' => 'jobs',
    'order' => 'DESC',
    'posts_per_page' => -1,
    'lang' => pll_current_language()
  );
  $loop = new WP_Query($args);
?>

  <?php
  if ($loop->have_posts()) :
  ?>
  <ul class="jobs-list">
  <?php
    while ($loop->have_posts()) :
      $loop->the_post();
      $location = get_field('work-place', get_the_ID());
  ?>
    <li class="jobs-item">
      <a href="<?php the_permalink(); ?>">
        <h3 class="jobs-title"><?php the_title(); ?></h3>
        | <span class="jobs-date"><?php echo get_the_date(); ?></span>
        <?php if ($location): ?>
        | <span class="jobs-location"><?php echo $location; ?></span>
        <?php endif; ?>
      </a>
    </li>
    <?php
    endwhile;
    ?>
  </ul>
  <?php
  endif;
  ?>
